#### Demos, based on Julien Simon's presentation and workshop on AWS Conference in Sofia, May 2019

### Link to presentation: 
https://www.slideshare.net/JulienSIMON5/build-train-and-deploy-machine-learning-models-at-scale-april-2019

### Link to workshop code: 
https://gitlab.com/juliensimon/ent321

### More examples:
https://github.com/awslabs/amazon-sagemaker-examples

### Setup Instructioons:


* Login to the AWS Console: https://console.aws.amazon.com.
* Select the US West (Oregon) region in the top right corner.
* Go to the SageMaker console: https://us-west-2.console.aws.amazon.com/sagemaker/home?region=us-west-2#/dashboard
* Go to "Notebook / Notebook instances".
* Click on "Create notebook instance".
    * "Notebook instance name": type a name for your instance.
    * "Notebook instance type": select _ml.t3.xlarge_. No need for anything bigger :)
    * "IAM role": select "Create a new role"
         * Select "Any S3 bucket".
         * Click on "Create role".
* In the "Git repositories" section:
    * Select "Clone a public Git repository" from the dropdown list.
    * In the "Git repository" box, enter: https://gitlab.com/didizlatkova/sage-maker-demos
* Leave all other boxes as is and click on "Create notebook instance". A few minutes later, the instance is listed as "In Service".
* Click on "Open Jupyter".
* Click on the notebook and get to work :)
